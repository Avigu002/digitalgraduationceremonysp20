﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CameraMovement : MonoBehaviour
{

    public GameObject aCamera;
    public VideoPlayer myVideo;
    public Transform myTransform;
    public Animator animator;

    bool areWeDone = false;


    // Update is called once per frame
    void Update()
    {

        if (myVideo.isPlaying == false && !areWeDone)
        {
            
            aCamera.transform.Translate(Vector3.back * 0.05f);
            if (Vector3.Distance(aCamera.transform.position, this.transform.position) < 1)
            {
                areWeDone = true;
            }


        }
    }
}
