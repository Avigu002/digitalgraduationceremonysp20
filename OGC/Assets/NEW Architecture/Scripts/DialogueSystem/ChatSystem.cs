﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Not being used currently.
public class ChatSystem : MonoBehaviour {

    public static ChatSystem Instance { get; set; }

    public List<string> dialogueLines = new List<string>();
    public string npcName;
    public GameObject dialoguePanel;
    
    Button continueButton;
    Text dialogueText, nameText;
    int dialogueIndex;
    bool endOfDialogue = false;

	// Use this for initialization
	void Awake () {

        //Getting UI components.
        continueButton = dialoguePanel.transform.Find("Continue").GetComponent<Button>();
        dialogueText = dialoguePanel.transform.Find("Text").GetComponent<Text>();
        nameText = dialoguePanel.transform.Find("Name").GetChild(0).GetComponent<Text>();

        

        continueButton.onClick.AddListener(delegate { ContinueDialogue(); });


        dialoguePanel.SetActive(false);
        

        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        

    }
	
    /**
     * @Parameters: lines of dialogue, name of NPC whos dialogue interaction is about to start. 
     * Starts dialogue system. 
     * 
     * **/
	public void AddDialogue(string [] lines, string nameOfNPC)
    {
        dialogueIndex = 0;
        dialogueLines = new List<string>(lines.Length);
        dialogueLines.AddRange(lines);

        npcName = nameOfNPC;

        CreateDialogue();
    }

    public void CreateDialogue()
    {
        dialogueText.text = dialogueLines[dialogueIndex];
        nameText.text = npcName;
        dialoguePanel.SetActive(true);
    }

    //Keeps dialogue system going. 
    public void ContinueDialogue()
    {
        if(dialogueIndex < dialogueLines.Count-1)
        {
            dialogueIndex++;
            dialogueText.text = dialogueLines[dialogueIndex];
        }
        else
        {
            endOfDialogue = true;
            dialoguePanel.SetActive(false);
            //CharacterManager.Instance.CursorState(true);
        }
    }

    public bool getEndOfDialogue()
    {
        return endOfDialogue;
    }
}
