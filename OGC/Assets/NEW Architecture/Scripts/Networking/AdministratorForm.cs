﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/*
 * Class is used to retrive info from online form.
 */
public class AdminstratorForm : MonoBehaviour
{

    public string filePath= "file ";
    void Start()
    {
        StartCoroutine(Upload(filePath));
    }

    IEnumerator Upload(string filePath)
    {
        byte[] myData = System.Text.Encoding.UTF8.GetBytes(filePath);
        UnityWebRequest www = UnityWebRequest.Put("http://www.localhost.com/upload", myData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Upload complete!");
        }
    }
}
