﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(fileName = "Speeches/ceremony speeches")]
public class CeremonySpeeches : ScriptableObject
{
    public List<Speech> beginningSpeeches;
    public List<Speech> endingSpeeches;

    public StudentRoster roster;
}

