﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class AudienceController : MonoBehaviour
{
    public Animator animator;
    public bool areWeDone = true;
    private float time = 0f;
    public float timeClapping = 20f;

    CeremonyManager manager;

    // Start is called before the first frame update
    void Start()
    {
        manager = CeremonyManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(areWeDone)
        {
            time += 0.01f;
            animator.SetTrigger("SpeechHasEnded");
            this.GetComponent<AudioSource>().enabled = true;

            if(time > timeClapping)
            {
                animator.SetTrigger("StopClapping");
                this.GetComponent<AudioSource>().enabled = false;
                areWeDone = false;
            }

        }
    }
}
