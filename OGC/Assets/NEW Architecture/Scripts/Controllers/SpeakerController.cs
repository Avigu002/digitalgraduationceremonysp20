﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeakerController : MonoBehaviour
{
    public Animator animator;
    public GameObject aStudent;
    public Vector3 targetPosition;
    CeremonyManager manager;

    bool areWeDone = true;
    // Start is called before the first frame update
    void Start()
    {
        manager = CeremonyManager.instance;
    }

    

    // Update is called once per frame
    public void UpdateSpeaker(bool isLineUp)
    {
        if (isLineUp && areWeDone)
        {
            animator.SetTrigger("WalkNow");
            targetPosition = new Vector3(transform.position.x - aStudent.transform.position.x, 0f, transform.position.z - aStudent.transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            aStudent.transform.rotation = Quaternion.Slerp(aStudent.transform.rotation, rotation, 0.05f);
            aStudent.transform.Translate(Vector3.forward * 0.05f);
        }
        else
        {
            animator.SetTrigger("");
        }    
    }
}
