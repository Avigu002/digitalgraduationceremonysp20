﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StudentLineUp : MonoBehaviour
{
    protected StudentRoster roster;
    protected Student tempStudent;
    protected NavMeshAgent currentAgent;
    Queue<Student> currentStudents = new Queue<Student>();

    public Transform onStageDestination;
    public Transform behindStageDestination;

    public GameObject currentGO;
    public ObjectPooler op;
    public Animator anim;
    public Vector3 targetVector;

    public bool isRosterFinished;
    public bool isFirstTime = true;
    public bool hasReachedStagePosition;
    public bool hasReachedOffStage;
    public bool hasFinishedWaiting = false;

    // Start is called before the first frame update
    public void StartLineUp(StudentRoster roster)
    {
        this.roster = roster;
        LoadStudentRoster(roster.roster);
        op = ObjectPooler.Instance;
        op.AddStudentsToPool(roster.roster);
        op.StartObjectPooler();
        LoadStudent(currentStudents.Dequeue());
        isRosterFinished = false;
        isFirstTime = false;
    }

    void LoadStudent(Student student)
    {
        HandleLineUpUI(student);
        currentGO = null;
        currentGO = op.SpawnFromPool(student.studentID, this.transform.position, this.transform.rotation);
        currentAgent = currentGO.GetComponent<NavMeshAgent>();
        anim = currentGO.GetComponent<Animator>();
        hasReachedOffStage = false;
        hasReachedStagePosition = false;
        GoToStage();

    }

    void LoadStudentRoster(Student[] students)
    {
        foreach(Student student in students)
        {
            currentStudents.Enqueue(student);
        }
    }

    public void GoToStage()
    {
        
        if (onStageDestination != null)
        {
            targetVector = onStageDestination.transform.position;
            currentAgent.SetDestination(targetVector);
            
        }
    }

    public void LeaveStage()
    {
        if (behindStageDestination != null)
        {
            targetVector = behindStageDestination.transform.position;
            currentAgent.SetDestination(targetVector);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentAgent != null && CheckDestinationReached(onStageDestination.position) && !hasReachedStagePosition && !hasReachedOffStage)
        {
    
            
            StartCoroutine(FadeInFadeOutUI());
            anim.Play("Celebrate");
            StartCoroutine(WaitForSeconds(5));
            if (hasFinishedWaiting)
            {
                LeaveStage();
                hasFinishedWaiting = false;
                hasReachedStagePosition = true;
                CeremonyManager.instance.currentState = CeremonyState.afterLineUp;
            }
                
        }
        else if (CheckDestinationReached(behindStageDestination.position) && hasReachedStagePosition && !hasReachedOffStage)
        {
            
            currentGO.SetActive(false);
            if (currentStudents.Peek() != null)
            {
                LoadStudent(currentStudents.Dequeue());
                hasReachedOffStage = true;
            }
            else
            {
                isRosterFinished = true;
                hasReachedOffStage = true;
            }

        }

        
    }

    public bool CheckDestinationReached(Vector3 target)
    {
        float distanceToTarget = Vector3.Distance(currentAgent.transform.position, target);
        if (distanceToTarget < 1)
        {
            print("Reached stage");
            return true;
        }
        return false;
    }

    public void HandleLineUpUI(Student student)
    {
        UIManager.instance.UpdateLineUpUI(student);
    }

    public IEnumerator FadeInFadeOutUI(float seconds = 2)
    {
        UIManager.instance.FadeInLineUpUI();
        yield return new WaitForSeconds(seconds);
        UIManager.instance.FadeOutLineUpUI();
    }

    public IEnumerator WaitForSeconds(float seconds = 5)
    {
        
        yield return new WaitForSeconds(seconds);
        hasFinishedWaiting = true;
    }
}

