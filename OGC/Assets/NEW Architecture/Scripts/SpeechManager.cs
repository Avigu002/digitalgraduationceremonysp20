﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class SpeechManager : MonoBehaviour
{

    //adding array of video players
    public int CurrentVideo;
    public VideoPlayer VideoPlayer;
    public AudioSource AudioSource;
    public List<VideoClip> VideoClips;
    public List<Speech> speeches = new List<Speech>();
    //public SpeechUIManager ui;
    
    public bool IsPlaying = false;
    public bool IsFinishedWithList;
    public bool isVideoDone;

    public int counter = 0;
  
    void Start()
    {
        CurrentVideo = 0;
    }

    void Update()
    {
        isVideoDone = isVideoFinished();
    }

    public bool isVideoFinished()
    {
        while (VideoPlayer.frame >= (long)VideoPlayer.frameCount -1)
        { 
                return true;
        }
        return false;
    }

    public void InitializeVideos(List<VideoClip> videos)
    {
        VideoClips = videos;
        VideoPlayer.clip = VideoClips[CurrentVideo];
        VideoPlayer.SetTargetAudioSource(0, AudioSource);
        IsFinishedWithList = false;
    }

    public void InitializeSpeechInfo(List<Speech> speeches)
    {
        this.speeches = speeches;   
    }

    public void HandleSpeechUI()
    {
        UIManager.instance.UpdateSpeechUI(this.speeches[CurrentVideo]);
    }

    public void Play()
    {
        HandleSpeechUI();
        VideoPlayer.Play();
        AudioSource.Play();
        IsPlaying = true;
        StartCoroutine(FadeInFadeOutUI());
    }
    public void Pause()
    {
        AudioSource.Pause();
        VideoPlayer.Pause();
        IsPlaying = false;

    }
    public void Next()
    {
        if (CurrentVideo == VideoClips.Count - 1)
        {
            CurrentVideo = 0;
            IsFinishedWithList = true;
            return;
        }
        else
        {
            CurrentVideo++;
        }
            
        VideoPlayer.clip = VideoClips[CurrentVideo];
        VideoPlayer.SetTargetAudioSource(0, AudioSource);
        if (IsPlaying)
            Play();
    }
    public void Prev()
    {
        if (CurrentVideo == 0)
            CurrentVideo = VideoClips.Count - 1;
        else
            CurrentVideo--;

        VideoPlayer.clip = VideoClips[CurrentVideo];
        VideoPlayer.SetTargetAudioSource(0, AudioSource);
        if (IsPlaying)
            Play();
    }
    public void Mute()
    {
        AudioSource.mute = true;
    }
    public void UnMute()
    {
        AudioSource.mute = false;
    }

    public void MuteToggle()
    {
        counter++;
        if (counter % 2 == 1)
        {
            VideoPlayer.SetDirectAudioMute(0,true);
        }
        else
        {
            VideoPlayer.SetDirectAudioMute(0, false);
        }
    }

    public void FullScreenToggle()
    {
            counter++;
            if (counter % 2 == 1)
            {
            VideoPlayer.renderMode = VideoRenderMode.CameraNearPlane; 
            }
            else
            {
            VideoPlayer.renderMode = VideoRenderMode.RenderTexture;
        }
    }

    public IEnumerator FadeInFadeOutUI(float seconds = 5)
    {
        UIManager.instance.FadeInSpeechUI();
        yield return new WaitForSeconds(seconds);
        UIManager.instance.FadeOutSpeechUI();
    }
}
