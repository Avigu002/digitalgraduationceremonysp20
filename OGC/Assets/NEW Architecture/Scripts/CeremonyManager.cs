﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CeremonyManager : MonoBehaviour
{
    public CeremonySpeeches speeches;
    //public StudentRoster students;
    public Speech currentSpeech;
    public SpeechManager sm;

    public SpeakerController speaker;

    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public VideoClip videoToPlay;

    public CeremonyState currentState;
    public StudentLineUp lineUpProcess;

    List<VideoClip> currentVideos = new List<VideoClip>();
    List<AudioClip> currentAudioClips = new List<AudioClip>();


    //Flags
    public bool isVidPlaying;
    public bool isVideoDone;
    public bool isStudentOnStage = false;
    public bool isLineUp;

    #region Singleton
    public static CeremonyManager instance;

    void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }
    #endregion

    void Start()
    {
        currentState = CeremonyState.beforeLineUp;
        SpeechSequencer(currentState);
        sm.InitializeVideos(currentVideos);
        sm.Play();
        isLineUp = false;
    }

    void Update()
    {
        if (sm.isVideoDone)
        {
            if (sm.IsFinishedWithList && currentState == CeremonyState.beforeLineUp)
            {
                LineUp();
            }
            else if(sm.IsFinishedWithList && currentState == CeremonyState.afterLineUp)
            {
                isLineUp = false;
                SpeechSequencer(currentState);
                sm.InitializeVideos(currentVideos);
                sm.Play();
            }
            else if (sm.IsFinishedWithList && currentState == CeremonyState.ending)
            {
                EndCeremony();
            }
            else
            {
                sm.Next();
            }
        }
    }

    public void LineUp()
    {
        if (lineUpProcess.isFirstTime)
        {
            isLineUp = true;
            Debug.Log("Students line up");
            speaker.UpdateSpeaker(true);
            lineUpProcess.StartLineUp(speeches.roster);
        }
        
        //currentState = CeremonyState.afterLineUp;
    }

    public void EndCeremony()
    {
        print("Congratulations!");
    }

    void PopulateCurrentLists(List<Speech> speeches)
    {
        currentVideos.Clear();
        currentAudioClips.Clear();

        for (int i = 0; i < speeches.Count; i++)
        {
            switch (speeches[i].type)
            {
                case TypeOfSpeech.video:
                    currentVideos.Add(speeches[i].video);
                    break;
                case TypeOfSpeech.audio:
                    currentAudioClips.Add(speeches[i].speech);
                    break;
                default:
                    break;
            }
        }
    }

    public void SpeechSequencer(CeremonyState state)
    {
        switch (state)
        {
            case CeremonyState.beforeLineUp:
                PopulateCurrentLists(speeches.beginningSpeeches);
                sm.InitializeSpeechInfo(speeches.beginningSpeeches);
                break;
            case CeremonyState.afterLineUp:
                PopulateCurrentLists(speeches.endingSpeeches);
                sm.InitializeSpeechInfo(speeches.endingSpeeches);
                break;
        }
    }
}
public enum CeremonyState
{
    beforeLineUp, afterLineUp, ending
}
