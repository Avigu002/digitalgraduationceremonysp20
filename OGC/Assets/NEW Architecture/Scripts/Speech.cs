﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(fileName = "Speeches/Speech")]
public class Speech : ScriptableObject
{
    public string speaker;
    public string speechTitle;
    public TypeOfSpeech type;

    public AudioClip speech;
    public VideoClip video;
    

    public Speech(string speaker, string speechTitle, AudioClip speech, VideoClip video)
    {
        this.speaker = speaker;
        this.speechTitle = speechTitle;
        this.speech = speech;
        this.video = video;
    }
}
public enum TypeOfSpeech
{
    audio, video
}
