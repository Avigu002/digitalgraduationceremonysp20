﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Students/Student")]
public class Student: ScriptableObject
{
    public string studentFirstName;
    public string studentLastName;
    public string studentID;
    public string degreeAwarded;
    public GameObject prefab;
    public Sprite studentImage;


    public Student(string fName, string lName, string ID, string dA, Sprite image, GameObject prefab)
    {
        studentFirstName = fName;
        studentLastName = lName;
        studentID = ID;
        degreeAwarded = dA;
        studentImage = image;
        this.prefab = prefab;
    }


}
