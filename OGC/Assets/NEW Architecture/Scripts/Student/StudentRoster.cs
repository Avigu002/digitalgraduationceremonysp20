﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Students/roster")]
public class StudentRoster : ScriptableObject
{
    public string department;
    public Student[] roster;
}
