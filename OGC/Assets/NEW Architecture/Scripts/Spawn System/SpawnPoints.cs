﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu(fileName = "New Spawn Point", menuName = "Spawn Points/Spawn Points")]
    public class SpawnPoints : ScriptableObject
    {
        public List<Vector3> facultySpawnPoints;
        public List<Vector3> studentSpawnPoints;
        public List<Vector3> miscSpawnPoints;

        public List<Vector3> SpawnDecider(string tag)
        {
            List<Vector3> listOfRandomSpawnPoints = new List<Vector3>();

            switch (tag)
            {
                case "faculty":
                    listOfRandomSpawnPoints = FacultySpawner();
                    break;
                case "student":
                    listOfRandomSpawnPoints = StudentSpawner();
                    break;
                case "misc":
                    listOfRandomSpawnPoints = MiscSpawner();
                    break;
            }

            if (listOfRandomSpawnPoints != null)
            {
                return listOfRandomSpawnPoints;
            }
            else
            {
                return null;
            }
        }

        public List<Vector3> RandomSpawnPoints(List<Vector3> spawnPoints)
        {
            int count = spawnPoints.Count;
            int last = count - 1;
            for (int i = 0; i < last; ++i)
            {
                int r = UnityEngine.Random.Range(i, count);
                var tmp = spawnPoints[i];
                spawnPoints[i] = spawnPoints[r];
                spawnPoints[r] = tmp;
            }
            return spawnPoints;
        }

        public List<Vector3> FacultySpawner()
        {
            List<Vector3> listOfRandomSpawnPoints = new List<Vector3>();
            listOfRandomSpawnPoints = RandomSpawnPoints(facultySpawnPoints);
            return listOfRandomSpawnPoints;
        }

        public List<Vector3> StudentSpawner()
        {
            List<Vector3> listOfRandomSpawnPoints = new List<Vector3>();
            listOfRandomSpawnPoints = RandomSpawnPoints(studentSpawnPoints);
            return listOfRandomSpawnPoints;
        }


        public List<Vector3> MiscSpawner()
        {
            List<Vector3> listOfRandomSpawnPoints = new List<Vector3>();
            listOfRandomSpawnPoints = RandomSpawnPoints(miscSpawnPoints);
            return listOfRandomSpawnPoints;
        }
    }