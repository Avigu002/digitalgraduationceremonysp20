﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class ObjectPooler : MonoBehaviour
    {

        [System.Serializable]
        public class Pool
        {
            public string tag;
            public GameObject prefab;
            public int size;

            public Pool(string tag, GameObject prefab, int size)
            {
                this.tag = tag;
                this.prefab = prefab;
                this.size = size;
            }
        }


        #region Singleton
        public static ObjectPooler Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public SpawnPoints spawnPoints;
        public List<Pool> poolsList;
        public Dictionary<string, Queue<GameObject>> _poolDictionary;

        public void StartObjectPooler()
        {
            _poolDictionary = new Dictionary<string, Queue<GameObject>>();

            foreach (Pool pool in poolsList)
            {
                Queue<GameObject> objectPool = new Queue<GameObject>();

                for (int i = 0; i < pool.size; i++)
                {

                    GameObject obj = Instantiate(pool.prefab);
                    


                    obj.SetActive(false);
                    objectPool.Enqueue(obj);
                }

                _poolDictionary.Add(pool.tag, objectPool);
            }
        }

    public void AddObjectToPool(GameObject prefab)
    {
        Pool tmp = new Pool("Student", prefab, 1);
        poolsList.Add(tmp);
    }

    public void AddStudentsToPool(Student[] prefabs)
    {
        for (int i = 0; i< prefabs.Length;i++)
        {
            Pool tmp = new Pool(prefabs[i].studentID, prefabs[i].prefab, 1);
            poolsList.Add(tmp);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
        {

            if (!_poolDictionary.ContainsKey(tag))
            {
                Debug.Log("Pool with tag: " + tag + " doesn't exist");
                return null;
            }

            GameObject objectToSpawn = _poolDictionary[tag].Dequeue();

            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;

            _poolDictionary[tag].Enqueue(objectToSpawn);
            Debug.Log("Object Spawned");
            return objectToSpawn;
        }

        public void SpawnSeveralObjectsInPool(int numberOfGameObjectsToSpawn, string tag)
        {
            List<Vector3> listOfSpawnPoints = new List<Vector3>();
            Quaternion rotation = new Quaternion(0, 0, 0, 0);

            listOfSpawnPoints = spawnPoints.SpawnDecider(tag);
            
            for (int i = 0; i < numberOfGameObjectsToSpawn; i++)
            {
                SpawnFromPool(tag, listOfSpawnPoints[i], rotation);
            }
        }
    }
