﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    #region Singleton
    public static UIManager instance;

    void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }
    #endregion

    [Header("Speech UI")]
    //public TMPro.text speakerName, speechName;
    public TMP_Text speakerName, speechName;
    public CanvasGroup group;

    [Header("Line Up UI")]
    public TMP_Text studentFname, studentLname, degreeAwarded;
    public Image studentImage;
    public CanvasGroup lineUpGroup;


    public void UpdateSpeechUI(Speech speech)
    {
        this.speakerName.text = speech.speaker;
        this.speechName.text = speech.speechTitle;
    }

    public void UpdateLineUpUI(Student student)
    {
        this.studentFname.text = student.studentFirstName;
        this.studentLname.text = student.studentLastName;
        this.degreeAwarded.text = student.degreeAwarded;
        this.studentImage.sprite = student.studentImage;
    }

    public void FadeOutSpeechUI()
    {
        StartCoroutine(FadeCanvasGroup(group, group.alpha, 0));
    }

    public void FadeInSpeechUI()
    {
        StartCoroutine(FadeCanvasGroup(group, group.alpha, 1));
    }

    public void FadeOutLineUpUI()
    {
        StartCoroutine(FadeCanvasGroup(lineUpGroup, lineUpGroup.alpha, 0));
    }

    public void FadeInLineUpUI()
    {
        StartCoroutine(FadeCanvasGroup(lineUpGroup, lineUpGroup.alpha, 1));
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup group, float start, float end, float lerp = 1.25f)
    {

        float _timeStartedLerping = Time.time;
        float _timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = _timeSinceStarted / lerp;

        while (true)
        {
            _timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = _timeSinceStarted / lerp;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            group.alpha = currentValue;

            if (percentageComplete >= 1)
                break;


            yield return new WaitForFixedUpdate();
        }
        print("done");
    }
}
