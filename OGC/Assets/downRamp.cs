﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class downRamp : MonoBehaviour
{
    public GameObject[] students;
    public GameObject nextCube;
    int i = 0;
    bool areWeDone = true;
    private int waitTime = 40;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (areWeDone)
        {
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - students[i].transform.position.x, transform.position.y - students[i].transform.position.y, transform.position.z - students[i].transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            students[i].transform.rotation = Quaternion.Slerp(students[i].transform.rotation, rotation, 0.05f);
            students[i].transform.Translate(Vector3.forward * 0.1f);
            //when distance between chair and character less than 0.8 isSitting will trigger
            if (Vector3.Distance(students[i].transform.position, this.transform.position) < 1)
            {

                students[i].transform.rotation = this.transform.rotation;
                nextCube.GetComponent<cubeScript6>().enabled = true;
                if (i < students.Length)
                {
                    students[i] = null;
                    StartCoroutine(corutine());

                }
                else
                {
                    areWeDone = false;
                }

            }
        }


    }
    IEnumerator corutine()
    {

        yield return new WaitForSeconds(waitTime);
        i++;


    }
}
