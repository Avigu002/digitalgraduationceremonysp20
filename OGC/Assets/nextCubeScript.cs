﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class nextCubeScript : MonoBehaviour
{
    public GameObject[] students;
    public GameObject nextCube;

    bool areWeDone = true;
    int i = 0;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Vector3 targetPosition;
        targetPosition = new Vector3(transform.position.x - students[i].transform.position.x, 0f, transform.position.z - students[i].transform.position.z);
        Quaternion rotation = Quaternion.LookRotation(targetPosition);
        students[i].transform.rotation = Quaternion.Slerp(students[i].transform.rotation, rotation, 0.05f);
        students[i].transform.Translate(Vector3.forward * 0.05f);
        if (Vector3.Distance(students[i].transform.position, this.transform.position) < 1.1)
        {


            students[i].transform.rotation = this.transform.rotation;
            nextCube.GetComponent<cornerPoint>().enabled = true;
            i++;


        }
    }
}
