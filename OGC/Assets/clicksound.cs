﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clicksound : MonoBehaviour
{
    public AudioSource sound;
    public AudioClip clickSound;
    public void ClickSound()
    {
        sound.PlayOneShot(clickSound);
    }
}
