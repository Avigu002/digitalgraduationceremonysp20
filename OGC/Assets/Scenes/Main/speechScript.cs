﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class speechScript : MonoBehaviour
{
    public GameObject aPresident;
    public VideoPlayer myVideo, myVideo1;
    public Transform myTransform;
    Vector3 myPosition;
    bool keepMoving = true;
    bool areWeDone = true;
    private float time = 0f;
    // public Animation myanimation;
    public float timeClapping = 1f;

    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {

        
        Debug.Log(myPosition);
    }

    // Update is called once per frame
    void Update()
    {

        
        if (areWeDone)
        {
            //myVideo.GetComponent<Renderer>().enabled = false;
            //myVideo1.GetComponent<Renderer>().enabled = false;
            animator.SetTrigger("ClapNow");
            time += 0.01f;
            if (time > timeClapping)
            {
                animator.SetTrigger("NoClap");
                Vector3 targetPosition;
                targetPosition = new Vector3(transform.position.x - aPresident.transform.position.x, 0f, transform.position.z - aPresident.transform.position.z);
                Quaternion rotation = Quaternion.LookRotation(targetPosition);
                aPresident.transform.rotation = Quaternion.Slerp(aPresident.transform.rotation, rotation, 0.05f);
                aPresident.transform.Translate(Vector3.forward * 0.05f);
                //when distance between chair and character less than 0.8 isSitting will trigger
                if (Vector3.Distance(aPresident.transform.position, this.transform.position) < 0.9)
                {
                    //studentAnimator.SetTrigger("isGoingUp");
                    aPresident.transform.rotation = this.transform.rotation;
                    areWeDone = false;
                    animator.SetTrigger("StartIdle");


                }
                
            }

            //moving and rotation character with chair
          

        }
           
    }


    }
