﻿using System.Collections;
using UnityEngine;

public class sitOn : MonoBehaviour
{
    //Our student object
    public GameObject aStudent;

    //component from student object
    Animator studentAnimator;

    //goToChair will be true when players clicks on a chair
    bool goToChair = false;

    //unity method that is called the user clicks
    //calls trigger isWalking from student animator controller
 
    private void OnMouseDown()
    {

        studentAnimator.SetTrigger("isWalking");
        goToChair = true;
    }
    
    void Start()
    {
        //Gets the animator from our student object
        //studentAnimator = aStudent.GetComponent<Animator>();
    }


    void Update()
    {
       /* 
        if(goToChair)
        {
            //moving and rotation character with chair
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - aStudent.transform.position.x, 0f, transform.position.z - aStudent.transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            aStudent.transform.rotation = Quaternion.Slerp(aStudent.transform.rotation, rotation, 0.05f);
            aStudent.transform.Translate(Vector3.forward * 0.01f);
            //when distance between chair and character less than 0.8 isSitting will trigger
            if(Vector3.Distance(aStudent.transform.position, this.transform.position) < 1.1)
            {
                studentAnimator.SetTrigger("isSitting");
                aStudent.transform.rotation = this.transform.rotation;

                goToChair = false;
            }
        }
        */

    }
}

