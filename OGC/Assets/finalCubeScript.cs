﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public class finalCubeScript : MonoBehaviour



{
    public GameObject[] students;
    public GameObject presCube;
    int i = 0;
    bool areWeDone = true;
    public VideoPlayer myVideo2, myVideo3;
    private int waitTime = 60;
    public GameObject announcment;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (areWeDone)
        {
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - students[i].transform.position.x, 0f, transform.position.z - students[i].transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            students[i].transform.rotation = Quaternion.Slerp(students[i].transform.rotation, rotation, 0.05f);
            students[i].transform.Translate(Vector3.forward * 0.05f);
            //when distance between chair and character less than 0.8 isSitting will trigger
            if (Vector3.Distance(students[i].transform.position, this.transform.position) < 1)
            {

               
                students[i].transform.rotation = this.transform.rotation;
                students[i].GetComponent<Animator>().SetTrigger("Clapping");
                if (i < students.Length)
                {
                    students[i] = null;
                    StartCoroutine(corutine());

                }
                else
                {
                    
                    areWeDone = false;
                }

              
               // myText.GetComponent<GUIText>().enabled = true;
               
                


            }
       

        }
        if (i == 2)
        {
            myVideo2.GetComponent<Renderer>().enabled = true;
            myVideo2.GetComponent<VideoPlayer>().enabled = true;
            myVideo3.GetComponent<Renderer>().enabled = true;
            myVideo3.GetComponent<VideoPlayer>().enabled = true;
            presCube.GetComponent<SpeakerController>().enabled = true;
            StartCoroutine(corutine2()); 
        }

    }
    IEnumerator corutine()
    {

        yield return new WaitForSeconds(waitTime);
        
        i++;
        Debug.Log(i);

    }
    IEnumerator corutine2()
    {

        yield return new WaitForSeconds(5);
        announcment.gameObject.SetActive(true);
        Debug.Log("message should appear");
     
      

    }
}

